import {Card} from "../../Deck/Card/Card";
import {Player} from "../../player";
import {Hand} from "./Hand";

export class PokerPlayer implements Player {
    _didWin: boolean;
    _bestHand: Hand;
    _draw: Card[] = [];

    constructor(draw: Card[]) {
        draw.forEach( card => this._draw.push(card));
    }

    set setDraw(cards: Card[]) {
        cards.forEach( card => this._draw.push(card));
    }

    get bestHand() {
        return this._bestHand;
    }

    set bestHand(bestHand: Hand) {
        this._bestHand = bestHand;
    }

    get draw() {
        return this._draw;
    }

}
