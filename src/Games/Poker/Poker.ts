import {Deck} from "../../Deck/Deck/Deck";
import {PokerPlayer} from "./PokerPlayer";
import {Card} from "../../Deck/Card/Card";
import {Game} from "../Game";
import {Pot} from "./Pot";

export class Poker implements Game {
    private _flop: Card[] = [];
    _pot: Pot;
    _deck: Deck;
    _players: PokerPlayer[] = [];

    constructor(numPlayers: number) {
        this._deck = new Deck();
        this._deck.shuffle();

        const flop: Card[] = this.deck.deal(3);
        flop.forEach( card => this._flop.push(card));
        this.burn();                        // burn one card
        this._flop.push(this.deck.draw());
        this.burn();                        // burn another card
        this._flop.push(this.deck.draw());  // river

        for (let i = 0; i < numPlayers; i++) {
            let draw = this._deck.deal(2);
            this._players[i] = new PokerPlayer(draw);
        }
        let allHands = [];

        this.players.forEach(player => {
            let pot = new Pot(this.flop, player.draw);
            pot.generateHands();
            player.bestHand = pot.getBestHand();
            allHands.push((player.bestHand));
        });

        this._pot = new Pot(this.flop, []);
        this._pot.hands = allHands;
        console.log("winning hand", this._pot.getBestHand());
    }

    get players(): PokerPlayer[] {
        return this._players;
    }

    get deck(): Deck {
        return this._deck;
    }

    get flop(): Card[] {
        return this._flop;
    }

    public burn(): void {
        this.deck.draw();
    }

    public determineWinner() {

    }

}
