import {Poker} from "./Poker";
import {Hand} from "./Hand";

let poker = new Poker(4);

// console.log(poker)

console.log("FLOP", poker.flop);
console.log("DRAW", poker.players[0].draw, poker.players[1].draw, poker.players[2].draw, poker.players[3].draw);
console.log("BEST HAND", poker.players[0].bestHand.hand, poker.players[1].bestHand.hand, poker.players[2].bestHand.hand, poker.players[3].bestHand.hand);
console.log("BEST HANDRANK", poker.players[0].bestHand.handRank, poker.players[1].bestHand.handRank, poker.players[2].bestHand.handRank, poker.players[3].bestHand.handRank);
