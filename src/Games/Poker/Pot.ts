import {Card} from "../../Deck/Card/Card";
import {handRankings} from "../../Deck/cards";
import {Hand} from "./Hand";

export class Pot {
    private readonly _hand: Card[] = [];
    private _hands: Hand[] = [];

    constructor(flop: Card[], draw: Card[]) {
        this._hand = [...flop, ...draw];
    }

    get hand() {
        return this._hand
    }

    get hands() {
        return this._hands;
    }

    set hands(hands: Hand[]) {
        hands.forEach( hand => this._hands.push(hand));
    }

    public getBestHand(): Hand {

        let bestHand = this.hands[0],
            bestHandRank = bestHand.handRank,
            besthandValue = bestHand.value;

        this.hands.slice(1).forEach(hand => {
            if (handRankings[bestHandRank] > handRankings[hand.handRank]) {
                bestHandRank = hand.handRank;
                besthandValue = hand.value;
                bestHand = hand;
            } else if (handRankings[bestHandRank] === handRankings[hand.handRank] && besthandValue < hand.value) {
                besthandValue = hand.value;
                bestHand = hand;
            }
        });

        return bestHand;
    }

    public generateHands() {
        let data  = new Array(5);
        // Print all combination using temprary
        // array 'data[]'
        this.getAllPossibleHands(this.hand,0, data, 0, this.hands);
    }

    public getAllPossibleHands(arr: Card[], index:number, data: Card[], i: number, perms: Hand[]): void {
        // Current hand is ready,
        // take snapshot
        if (index == data.length) {
            let hand = new Hand([...data.slice()]);
            perms.push(hand);
            return;
        }

        // When no more elements are there to put in data[]
        if (i >= arr.length)
            return;

        // current is included, put next at next
        // location
        data[index] = arr[i];
        this.getAllPossibleHands(arr, index + 1, data, i + 1, this.hands);

        // current is excluded, replace it with
        // next (Note that i+1 is passed, but
        // index is not changed)
        this.getAllPossibleHands(arr, index, data, i + 1, this.hands);
    }

}

