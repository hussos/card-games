import {Card} from "../../Deck/Card/Card";
import {rankings} from "../../Deck/cards";

export class Hand {

    private _handMap = new Map();
    private _hand: Card[] = [];
    private _handRank;
    private _value;

    constructor(hand: Card[]) {
        this._hand = hand;
        this._hand.sort((a,b) => rankings[a.rank] - rankings[b.rank]);

        this._hand.forEach(card => {
            if (!this._handMap.has(card.rank)) {
                this._handMap.set(card.rank, 1)
            } else {
                let rank = this._handMap.get(card.rank);
                this._handMap.set(card.rank, rank + 1);
            }

            this.handRank = 0;
            this.value = 0;
        });

    }

    get hand() {
        return this._hand;
    }

    set hand(cards: Card[]) {
        cards.forEach(card => this._hand.push(card));
    }

    get handRank() {
        return this._handRank;
    }

    get value() {
        return this._value;
    }

    set handRank(rank) {

        if (this.isRoyalFlush()) {
            this._handRank = 'royalFlush';
            return;
        }
        if (this.isStraightFlush()){
            this._handRank = 'straightFlush';
            return;
        }
        if (this.isFourOfAKind()) {
            this._handRank = 'fourOfAKind';
            return;
        }
        if (this.isFullHouse()) {
            this._handRank = 'fullHouse';
            return;
        }
        if (this.isFlush()) {
            this._handRank = 'flush';
            return;
        }
        if (this.isStraight()) {
            this._handRank = 'straight';
            return;
        }
        if (this.isThreeOfAKind()) {
            this._handRank = 'threeOfAKind';
            return;
        }
        if (this.isTwoPair()) {
            this._handRank = 'twoPair';
            return;
        }
        if (this.isOnePair()) {
            this._handRank = 'onePair';
            return;
        }

        this._handRank = 'highCard';
    }

    set value(v) {
        let value = 0;

        this.hand.forEach(card => value += rankings[card.rank]);

        this._value = value;
        return;
    }

    public isOnePair() {
        return this.isNOfAKind(2, 1);
    }

    public isTwoPair() {
        return this.isNOfAKind(2, 2);
    }

    public isFullHouse() {
        return this.isNOfAKind(2, 1) && this.isNOfAKind(3, 1);
    }

    public isFourOfAKind() {
        return this.isNOfAKind(4, 1);
    }

    public isThreeOfAKind() {
        return this.isNOfAKind(3, 1) && !this.isFullHouse();
    }

    public isStraightFlush() {
        return this.isFlush() && this.isStraight();
    }

    public isNOfAKind(kind, occurence) {
        let occurenceCount = 0;

        for (var [key, val] of this._handMap)
            if (val === kind)
                occurenceCount++;

        return occurenceCount === occurence;
    }

    //TODO: edge case for 'wheel'
    public isStraight() {
        let prevRank = this.hand[0].rank,
            prevVal = rankings[prevRank],
            wheel = ['2', '3', '4', '5', 'a'],
            wheelCounter = 0,
            isValid = true;

        for (let i = 0; i < this.hand.length; i++)
            if (this.hand[i].rank === wheel[i])
                wheelCounter++;

        for (let i = 1; i < this.hand.length; i++) {
            let currRank = this.hand[i].rank;
            if (rankings[currRank] - prevVal !== 1)
                isValid = false;
            prevVal = rankings[currRank];
        }

        return isValid || wheelCounter === 5;
    }

    public isFlush() {
        let prevSuit = this.hand[0].suit;

        for (let i = 1; i < this.hand.length; i++) {
            if (this.hand[i].suit !== prevSuit)
                return false;
            prevSuit = this.hand[i].suit;
        }

        return true;
    }

    public isRoyalFlush() {
        let royalFlush = ['10', 'j', 'q', 'k', 'a'],
            isValid = true;

        for (let i = 0; i < this.hand.length; i++)
            if (this.hand[i].rank !== royalFlush[i])
                isValid = false;

        return isValid && this.isStraightFlush()
    }

}
