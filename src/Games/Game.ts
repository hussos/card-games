import {PokerPlayer} from "./Poker/PokerPlayer";
import {Deck} from "../Deck/Deck/Deck";


export interface Game {
    _deck: Deck;
    _players: PokerPlayer[];
}
