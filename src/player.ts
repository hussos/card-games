import {Card} from "./Deck/Card/Card";

export interface Player {
    _draw: Card[];
    _didWin: boolean;
}
