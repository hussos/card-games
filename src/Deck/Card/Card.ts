export class Card {

    private _suit: string;
    private _value: number;
    private _rank: string;

    constructor(suit, value, rank) {
        this._rank = rank;
        this._suit = suit;
        this._value = value;
    }

    get suit() {
        return this._suit;
    }

    get value() {
        return this._value;
    }

    get rank() {
        return this._rank;
    }

    public static createFromJSON(rank, value, suit) {
        return new Card(suit, value, rank);
    }

}

