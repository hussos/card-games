export declare class Card {
    private _suit;
    private _value;
    private _rank;
    constructor(suit: any, value: any, rank: any);
    get suit(): string;
    get value(): number;
    get rank(): string;
    static createFromJSON(rank: any, value: any, suit: any): Card;
}
